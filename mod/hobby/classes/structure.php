<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains class mod_feedback_structure
 *
 * @package   mod_hobby
 * @copyright Lam Tran
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Stores and manipulates the structure of the feedback or template (items, pages, etc.)
 *
 * @package   mod_hobby
 * @copyright Lam Tran
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_hobby_structure {
    /** @var stdClass record from 'hobby' table.
     * Reliably has fields: id, course, timeopen, timeclose, anonymous, completionsubmit.
     * For full object or to access any other field use $this->get_hobby()
     */
    protected $hobby;
    /** @var cm_info */
    protected $cm;
    /** @var int course where the feedback is filled. For feedbacks that are NOT on the front page this is 0 */
    protected $courseid = 0;
    /** @var stdClass */
    protected $item;
    protected $allitems;
    /** @var array */
    protected $allcourses;
    /** @var int */
    protected $userid;

   /**
    * mod_hobby_structure constructor.
    *
    * @param $hobby
    * @param $cm
    * @param int $courseid
    * @param null $templateid
    * @param int $userid
    * @throws coding_exception
    * @throws moodle_exception
    */
    public function __construct($hobby, $cm, $courseid, $userid = 0) {
        global $USER, $DB;

        if ((empty($hobby->id) || empty($hobby->course)) && (empty($cm->instance) || empty($cm->course))) {
            throw new coding_exception('Either $hobby or $cm must be passed to constructor');
        }
        $this->hobby = $hobby ?: $DB->get_record('hobby', ['id' => $cm->instance, 'course' => $cm->course]);
        if ($cm && $cm instanceof cm_info) {
            $this->cm = $cm;
        }
        else {
            $this->cm = get_fast_modinfo($this->hobby->course)->instances['hobby'][$this->hobby->id];
        }

        $this->courseid = $courseid ? $courseid : $this->hobby->course;

        if (empty($userid)) {
            $this->userid = $USER->id;
        } else {
            $this->userid = $userid;
        }

        if (!$hobby) {
            // handle something here
        }
    }

    /**
     * @return mixed
     * @throws dml_exception
     */
    public function get_hobby() {
        global $DB;
        if (!isset($this->hobby->id) && !isset($this->hobby->name)) {
            // Make sure the full object is retrieved.
            $this->hobby = $DB->get_record('hobby', ['id' => $this->hobby->id], '*', MUST_EXIST);
        }
        return $this->hobby;
    }

    /**
     * Current course module
     * @return stdClass
     */
    public function get_cm() {
        return $this->cm;
    }

    /**
     * @return int
     */
    public function get_courseid() {
        return $this->courseid;
    }

    /**
     * @return stdClass
     * @throws dml_exception
     */
    public function get_hobby_item() {
        global $DB;
        if ($this->item === null) {
            $this->item = $DB->get_record('hobby_item', [
                'hobby' => $this->hobby->id,
                'userid' => $this->userid
            ], '*');
        }
        return $this->item;
    }

    /**
     * Get fresh hobby item
     * @return mixed|stdClass
     * @throws dml_exception
     */
    protected function refresh_hobby_item() {
        global $DB;
        $this->item = $DB->get_record('hobby_item', [
            'hobby' => $this->hobby->id,
            'userid' => $this->userid
        ], '*');
        return $this->item;
    }

    /**
     * @return array
     * @throws dml_exception
     */
    public function get_hobby_items() {
        global $DB;
        if ($this->allitems === null) {
            $sql = "SELECT 
                hi.id,
                hi.name,
                hi.email,
                hi.phone,
                hi.intro, 
                concat(u.firstname, ' ', u.lastname) as fullname
                FROM {hobby_item} hi
                JOIN {user} u ON u.id = hi.userid
                WHERE hi.hobby = :hobby";
            $this->allitems = $DB->get_records_sql($sql, [
                'hobby' => $this->hobby->id
            ]);
        }
        return $this->allitems;
    }

    /**
     * @param int $groupid
     * @return mixed
     * @throws dml_exception
     */
    public function count_completed_responses($groupid = 0) {
        global $DB;
        if (intval($groupid) > 0) {
            $query = "SELECT COUNT(DISTINCT fbc.id)
                        FROM {hobby_item} hi, {groups_members} gm
                        WHERE hi.hobby = :hobby
                            AND gm.groupid = :groupid
                            AND hi.userid = gm.userid";
        } else if ($this->courseid) {
            $query = "SELECT COUNT(hi.id)
                        FROM {hobby_item} hi
                        JOIN {hobby} h ON h.id = hi.hobby
                        WHERE hi.hobby = :hobby
                            AND h.course = :courseid";
        } else {
            $query = "SELECT COUNT(hi.id) FROM {hobby_item} hi WHERE hi.hobby = :hobby";
        }
        $params = ['hobby' => $this->hobby->id, 'groupid' => $groupid, 'courseid' => $this->courseid];
        return $DB->get_field_sql($query, $params);
    }
}
