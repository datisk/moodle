<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains class mod_feedback\output\summary
 *
 * @package   mod_feedback
 * @copyright 2016 Marina Glancy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_hobby\output;

use renderable;
use templatable;
use renderer_base;
use stdClass;
use moodle_url;
use mod_hobby_structure;

/**
 * Class to help display feedback summary
 *
 * @package   mod_hobby
 * @copyright Lam Tran
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class summary implements renderable, templatable {

    /** @var mod_hobby_structure */
    protected $hobbystructure;

    /** @var int */
    protected $mygroupid;

    /** @var bool  */
    protected $extradetails;

    /**
     * Constructor.
     *
     * @param mod_hobby_structure $hobbystructure
     * @param int $mygroupid currently selected group
     * @param bool $extradetails display additional details (time open, time closed)
     */
    public function __construct($hobbystructure, $mygroupid = false, $extradetails = false) {
        $this->hobbystructure = $hobbystructure;
        $this->mygroupid = $mygroupid;
        $this->extradetails = $extradetails;
    }

    /**
     * @param renderer_base $output
     * @return array|stdClass
     * @throws \dml_exception
     */
    public function export_for_template(renderer_base $output) {
        $r = new stdClass();
        $r->completedcount = $this->hobbystructure->count_completed_responses($this->mygroupid);
        $r->items = array_values($this->hobbystructure->get_hobby_items());
        $r->itemscount = count($r->items);
        return $r;
    }
}
