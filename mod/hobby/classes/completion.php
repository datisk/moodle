<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains class mod_feedback_completion
 *
 * @package   mod_hobby
 * @copyright Lam Tran
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Collects information and methods about hobby completion (either complete.php or show_entries.php)
 *
 * @package   mod_hobby
 * @copyright Lam Tran
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_hobby_completion extends mod_hobby_structure {

    /** @var moodleform */
    protected $form = null;

    /**
     * @return bool
     * @throws coding_exception
     */
    public function can_complete() {
        global $CFG;
        global $USER;
        $context = context_module::instance($this->cm->id);
        if (has_capability('mod/hobby:complete', $context, $this->userid)) {
            return true;
        }

        if ($this->hobby->course == SITEID AND ($USER->id == $this->userid)) {
            return true;
        }

        return false;
    }

    /**
     * @throws coding_exception
     */
    public function trigger_module_viewed() {
        $event = \mod_hobby\event\course_module_viewed::create_from_record($this->hobby, $this->cm, $this->cm->get_course());
        $event->trigger();
    }

    /**
     * @return mixed
     */
    public function render_items() {
        return $this->form->render();
    }

    /**
     * @return moodle_url|null
     * @throws coding_exception
     * @throws moodle_exception
     */
    public function process_page() {
        global $CFG, $PAGE, $SESSION;

        $urltogo = null;

        if (!$item = $this->get_hobby_item()) {
            $item = new stdClass();
            $item->name = $item->email = $item->phone = $item->intro = null;
        }

        $this->form = new mod_hobby_complete_form($this, 'hobby_complete_form');
        $this->form->set_data([
           'name' => $item->name,
           'email' => $item->email,
           'phone' => $item->phone,
           'intro' => ['text' => $item->intro]
        ]);

        if ($this->form->is_cancelled()) {
            // Form was cancelled - return to the course page.
            $urltogo = course_get_url($this->courseid ?: $this->hobby->course);
        } else if ($this->form->is_submitted() && $this->form->is_validated()) {
            $data = $this->form->get_submitted_data();
            $this->update_form_data($data);
            $urltogo = course_get_url($this->courseid ?: $this->hobby->course);
        }
        return $urltogo;
    }

    /**
     * @param $payload
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function update_form_data(object $payload) {
        global $DB;

        $hobby = $this->get_hobby();
        $item = $this->get_hobby_item();

        if (!$item) {
            $item = new stdClass();
            $item = $this->fill_data($item, $payload);
            $item->userid = $this->userid;
            $item->hobby = $hobby->id;
            $item->timecreated = $item->timemodified = time();
            $DB->insert_record('hobby_item', $item);
        }
        else {
            $item = $this->fill_data($item, $payload);
            $item->timemodified = time();
            $DB->update_record('hobby_item', $item);
        }

        $this->refresh_hobby_item();
    }

    public function fill_data($arr, $form) {
        $map = ['name', 'email', 'phone', 'intro'];
        foreach($map as $field) {
            if (!isset($form->$field)) continue;
            if ($field === 'intro') {
                $arr->intro = $form->intro['text'];
                continue;
            }
            $arr->$field = $form->$field;
        }
        return $arr;
    }
}
