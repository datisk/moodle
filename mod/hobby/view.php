<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * the first page to view the feedback
 *
 * @author Lam Tran
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_hobby
 */

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/hobby/lib.php');
global $USER;
try {
    $id = required_param('id', PARAM_INT);
    $courseid = optional_param('courseid', false, PARAM_INT);

    list($course, $cm) = get_course_and_cm_from_cmid($id, 'hobby');
    require_course_login($course, true, $cm);

    $hobby = $PAGE->activityrecord;

    $context = context_module::instance($cm->id);

    // Check access to the given courseid.
    if ($courseid AND $courseid != SITEID) {
        require_course_login(get_course($courseid)); // This overwrites the object $COURSE .
    }

    $hobbycompletion = new mod_hobby_completion($hobby, $cm, $courseid);
    // Trigger module viewed event.
    $hobbycompletion->trigger_module_viewed();

    if ($course->id == SITEID) {
        $PAGE->set_pagelayout('incourse');
    }
    $PAGE->set_url('/mod/hobby/view.php', array('id' => $cm->id));
    $PAGE->set_title($hobby->name);
    $PAGE->set_heading($course->fullname);

    /// Print the page header
    echo $OUTPUT->header();

    echo $hobby->name;
    echo $hobby->intro;

    if (has_capability('mod/hobby:edititems', $context)) {
        echo $OUTPUT->heading(get_string('overview', 'hobby'), 3);
        //get the groupid
        $groupselect = groups_print_activity_menu($cm, $CFG->wwwroot.'/mod/hobby/view.php?id='.$cm->id, true);
        $mygroupid = groups_get_activity_group($cm);

        echo $groupselect.'<div class="clearer">&nbsp;</div>';
        $summary = new mod_hobby\output\summary($hobbycompletion, $mygroupid, true);
        echo $OUTPUT->render_from_template('mod_hobby/summary', $summary->export_for_template($OUTPUT));
    }

    if (has_capability('mod/hobby:complete', $context)) {
        if ($hobbycompletion->can_complete()) {
            echo $OUTPUT->box_start('generalbox boxaligncenter');
            // Display a link to complete feedback or resume.
            $completeparams = ['id' => $id];
            if ($courseid) {
                $completeparams['courseid'] = $courseid;
            }
            $completeurl = new moodle_url('/mod/hobby/complete.php', $completeparams);
            $label = get_string('complete_the_form', 'hobby');
            echo html_writer::div(html_writer::link($completeurl, $label, array('class' => 'btn btn-secondary')), 'complete-hobby');
            echo $OUTPUT->box_end();
        }
    }

    echo $OUTPUT->footer();
}
catch (\Exception $e) {
    throw $e;
}

