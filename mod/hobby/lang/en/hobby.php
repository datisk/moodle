<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_hobby
 * @category    string
 * @copyright   2019 Lam Tran
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'hobby';
$string['modulename'] = 'Hobby';
$string['modulenameplural'] = 'Hobbies';
$string['hobbyname'] = 'Name';
$string['hobbyphone'] = 'Phone';
$string['hobbyemail'] = 'Email';
$string['hobbyintro'] = 'Introduction';
$string['hobbyemailformat'] = 'Field must be email format';
$string['hobbyphoneformat'] = 'Field must be phone format';
$string['pluginadministration'] = 'Hobby administration';
$string['hobby:addinstance'] = 'Add new hobby';
$string['hobby:view'] = 'View a hobby';
$string['cannotaccess'] = 'You can only access this hobby from a course';
$string['overview'] = 'Overview';
$string['completed_hobbies'] = 'Completed Hobbies';
$string['complete_the_form'] = 'Fill your hobby';
$string['hobby:complete'] = 'Complete a hobby';
$string['hobby:save'] = 'Save';

$string['hobby:deletesubmissions'] = 'Delete completed submissions';
$string['hobby:edititems'] = 'Edit items';
$string['hobby:mapcourse'] = 'Map hobby to courses';
$string['hobby:viewanalysepage'] = 'View the analysis page after submit';
$string['hobby:viewreports'] = 'View reports';


$string['privacy:metadata:hobby_item'] = 'Information about user hobby. This includes when a user has chosen to provide personal data';
$string['privacy:metadata:hobby_item:userid'] = 'The ID of the user';
$string['privacy:metadata:hobby_item:name'] = 'The name of the user';
$string['privacy:metadata:hobby_item:email'] = 'The email of the user';
$string['privacy:metadata:hobby_item:phone'] = 'The phone of the user';
$string['privacy:metadata:hobby_item:intro'] = 'The introduction of the user';
