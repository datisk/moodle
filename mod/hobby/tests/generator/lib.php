<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * mod_hobby data generator class.
 *
 * @package    mod_hobby
 * @category   test
 * @copyright  Lam Tran
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_hobby_generator extends testing_module_generator {

    public function generate_create_hobby_data($course) {
        return [
            'course' => $course->id,
            'name' => 'Hobby',
            'intro' => 'Hobby test'
        ];
    }

    public function generate_update_hobby_data($course) {
        return [
            'course' => $course->id,
            'name' => 'Hobby Updated',
            'intro' => 'Hobby test updated'
        ];
    }

    public function generate_create_hobby_item_data($hobby) {
        return  [
            'name' => 'Lam',
            'email' => 'tranbaolam6693@gmail.com',
            'phone' => '0123456789',
            'hobby' => $hobby->id,
            'intro' => 'Intro'
        ];
    }

    public function generate_update_hobby_item_data($hobby) {
        return  [
            'name' => 'Lam updated',
            'email' => 'tranbaolam66931@gmail.com',
            'phone' => '01234567891',
            'hobby' => $hobby->id,
            'intro' => 'Intro updated'
        ];
    }

    /**
     * @param $id
     * @return mixed
     * @throws dml_exception
     */
    public function get_hobby_by_id($id) {
        global $DB;
        return $DB->get_record('hobby', array('id' => $id));
    }

    /**
     * @param $hobby
     * @return object
     * @throws dml_exception
     */
    public function create_hobby_item($hobby) {
        global $DB;
        $record = $this->generate_create_hobby_item_data($hobby);
        $id = $DB->insert_record('hobby_item', (object)$record);
        if($id) {
            return (object)array_merge(['id' => $id], $record);
        }
    }

    /**
     * @param $hobby
     * @param $user
     * @return mixed
     * @throws dml_exception
     */
    public function get_hobby_item($hobby) {
        global $DB, $USER;
        return $DB->get_record('hobby_item', [
                'hobby' => $hobby->id,
                'userid' => $USER->id
        ], '*');
    }
}
