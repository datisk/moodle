<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Unit tests for (some of) mod/feedback/lib.php.
 *
 * @package    mod_hobby
 * @copyright  Lam Tran
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->dirroot . '/mod/hobby/lib.php');

/**
 * Unit tests for (some of) mod/hobby/lib.php.
 *
 * @copyright  Lam Tran
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_hobby_lib_testcase extends advanced_testcase {

    /**
     * @throws coding_exception
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function test_hobby_initialise() {
        $this->resetAfterTest();
        $this->setAdminUser();

        $course = $this->getDataGenerator()->create_course();

        $hobby = $this->getDataGenerator()->create_module('hobby', ['course' => $course->id]);

        // Test different ways to construct the structure object.
        $pseudocm = get_coursemodule_from_instance('hobby', $hobby->id); // Object similar to cm_info.
        $cm = get_fast_modinfo($course)->instances['hobby'][$hobby->id]; // Instance of cm_info.

        $constructorparams = [
            [$hobby, null],
            [null, $pseudocm],
            [null, $cm],
            [$hobby, $pseudocm],
            [$hobby, $cm],
        ];

        foreach ($constructorparams as $params) {
            $structure = new mod_hobby_completion($params[0], $params[1], $course->id);
            $this->assertTrue($structure->can_complete());
            $this->assertTrue($structure->get_cm() instanceof cm_info);
            $this->assertEquals($hobby->course, $structure->get_courseid());
            $structureintro =  $structure->get_hobby() ?  $structure->get_hobby()->intro : null;
            $this->assertEquals($hobby->intro, $structureintro);
        }
    }

    /**
     * Test hobby activity create by hobby_add_instance
     * @throws moodle_exception
     */
    public function test_hobby_add_instance() {
        $this->resetAfterTest();
        $this->setAdminUser();
        /** @var mod_hobby_generator|testing_data_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_hobby');

        // create course
        $course = $this->getDataGenerator()->create_course();
        // generate data for hobby
        $params = $generator->generate_create_hobby_data($course);
        // create hobby
        $id = hobby_add_instance((object) $params);

        $model = $generator->get_hobby_by_id($id);

        // check if exist
        $this->assertNotNull($model);
        // check if correct insert data
        $this->assertEquals($id, $model->id);
        $this->assertEquals($params['name'], $model->name);
        $this->assertEquals($params['intro'], $model->intro);
    }

    /**
     * Test hobby update by hobby_update_instance
     * @throws coding_exception
     * @throws dml_exception
     */
    public function test_hobby_update_instance() {
        $this->resetAfterTest();
        $this->setAdminUser();
        /** @var mod_hobby_generator|testing_data_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_hobby');

        // create course
        $course = $this->getDataGenerator()->create_course();
        // create  hobby
        $hobby = $this->getDataGenerator()->create_module('hobby', [
            'course' => $course->id
        ]);
        $params = $generator->generate_update_hobby_data($course);
        $params['instance'] = $hobby->id;
        // wait to get different time modified
        $this->waitForSecond();
        hobby_update_instance((object) $params);

        // get updated model
        $updatedmodel = $generator->get_hobby_by_id($hobby->id);

        $this->assertEquals($params['name'], $updatedmodel->name);
        $this->assertEquals($params['intro'], $updatedmodel->intro);
        $this->assertTrue($updatedmodel->timemodified > $hobby->timemodified);

    }

    /**
     * Test hobby delete by hobby_delete_instance
     * @throws moodle_exception
     */
    public function test_hobby_delete_instance() {
        $this->resetAfterTest();
        $this->setAdminUser();

        /** @var mod_hobby_generator|testing_data_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_hobby');

        // create course
        $course = $this->getDataGenerator()->create_course();
        // create  hobby
        $hobby = $this->getDataGenerator()->create_module('hobby', [
            'course' => $course->id
        ]);
        // create hobby item
        $generator->create_hobby_item($hobby);

        //delete hobby and all items
        hobby_delete_instance($hobby->id);

        // check if hobby is deleted
        $exist = $generator->get_hobby_by_id($hobby->id);
        $this->assertFalse($exist);

        // check if hobby item is deleted
        $existitem = $generator->get_hobby_item($hobby);
        $this->assertFalse($existitem);

    }

    /**
     * @throws coding_exception
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function test_hobby_create_item() {
        $this->resetAfterTest();
        $this->setAdminUser();

        /** @var mod_hobby_generator|testing_data_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_hobby');

        // create course
        $course = $this->getDataGenerator()->create_course();
        // create  hobby
        $hobby = $this->getDataGenerator()->create_module('hobby', [
                'course' => $course->id
        ]);
        // get course module
        $cm = get_fast_modinfo($course)->instances['hobby'][$hobby->id]; // Instance of cm_info.
        // create hobby item
        $structure = new mod_hobby_completion($hobby, $cm, $course->id);
        $params = $generator->generate_create_hobby_item_data($hobby);

        //transform to form
        $form = (object)$params;
        $form->intro = ['text' => $form->intro];

        // add hobby item
        $structure->update_form_data($form);

        // get hobby item again
        $item = $structure->get_hobby_item();

        foreach($params as $key => $value) {
            $this->assertEquals($value, $item->$key);
        }
        $this->assertEquals($form->intro['text'], $item->intro);

    }

    /**
     * Test hobby item can be updated
     * @throws coding_exception
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function test_hobby_update_item() {
        $this->resetAfterTest();
        $this->setAdminUser();

        /** @var mod_hobby_generator|testing_data_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_hobby');

        /** @var mod_hobby_generator|testing_data_generator $hobbygenerator */
        $hobbygenerator = $this->getDataGenerator()->get_plugin_generator('mod_hobby');

        // create course
        $course = $this->getDataGenerator()->create_course();
        // create  hobby
        $hobby = $this->getDataGenerator()->create_module('hobby', [
            'course' => $course->id
        ]);
        // get course module
        $cm = get_fast_modinfo($course)->instances['hobby'][$hobby->id]; // Instance of cm_info.
        // create hobby item
        $hobbygenerator->create_hobby_item($hobby);

        $structure = new mod_hobby_completion($hobby, $cm, $course->id);

        $updatedparams = $generator->generate_update_hobby_item_data($hobby);
        //transform to form
        $form = (object)$updatedparams;
        $form->intro = ['text' => $form->intro];
        // update hobby item
        $structure->update_form_data($form);

        // refresh hobby item
        $currenthobbyitem = $structure->get_hobby_item();

        foreach($updatedparams as $key => $value) {
            $this->assertEquals($value, $currenthobbyitem->$key);
        }
        $this->assertEquals($form->intro['text'], $currenthobbyitem->intro);

    }



}
