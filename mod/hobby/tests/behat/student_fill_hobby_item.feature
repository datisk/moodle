@mod @mod_hobby
Feature: Here comes a description of your user story.

Background:
  Given the following "courses" exist:
    | fullname | shortname | category | groupmode |
    | Course 1 | C1        | 0        | 1         |
  And the following "users" exist:
    | username      | firstname | lastname | email                    |
    | teacherbehat  | Teacher   | Behat    | teacherbehat@example.com |
    | studentbehat  | Student   | Behat    | studentbehat@example.com |
    | studentbehat2 | Student 2 | Behat    | studentbehat2@example.com |
  And the following "course enrolments" exist:
    | user          | course | role           |
    | studentbehat  | C1     | student        |
    | studentbehat2 | C1     | student        |
    | teacherbehat  | C1     | editingteacher |
  And the following "activities" exist:
    | activity   | name            | course               | idnumber  |
    | hobby      | Site hobby      | C1                   | hobby1    |

Scenario: Student 1 can fill hobby item, and teacher can view it but Student 2 cannot view Student 1.
  When I log in as "studentbehat"
  And I am on "Course 1" course homepage
  And I follow "Site hobby"
  And I click on "Fill your hobby" "link"
  And I should see "Site hobby"
  And I set the following fields to these values:
    | name          | Hobby Item          |
    | email         | hobbyitem@gmail.com |
    | phone         | 0123456789          |
    | intro[text]  | test introduction   |
  And I click on "Save" "button"
  And I should see "Course 1"
  And I log out

  When I log in as "teacherbehat"
  And I am on "Course 1" course homepage
  And I follow "Site hobby"
  And I should see "Overview"
  And I should see "Hobby Item"
  And I should see "hobbyitem@gmail.com"
  And I should see "0123456789"
  And I should see "test introduction"
  And I log out

  When I log in as "studentbehat2"
  And I am on "Course 1" course homepage
  And I follow "Site hobby"
  And I should not see "Overview"
  And I should not see "Hobby Item"
  And I should not see "hobbyitem@gmail.com"
  And I should not see "0123456789"
  And I should not see "test introduction"
  And I log out

