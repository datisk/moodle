<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @author Lam Tran
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_hobby
 */

require_once("../../config.php");
require_once("lib.php");

try {

    $id = required_param('id', PARAM_INT);
    $courseid = optional_param('courseid', null, PARAM_INT);

    list($course, $cm) = get_course_and_cm_from_cmid($id, 'hobby');
    $hobby = $DB->get_record("hobby", array("id" => $cm->instance), '*', MUST_EXIST);

    $urlparams = array('id' => $cm->id, 'courseid' => $courseid);
    $PAGE->set_url('/mod/hobby/complete.php', $urlparams);

    require_course_login($course, true, $cm);

    $PAGE->set_activity_record($hobby);

    $context = context_module::instance($cm->id);

    $hobbycompletion = new mod_hobby_completion($hobby, $cm, $courseid);

    $courseid = $hobbycompletion->get_courseid();
    //check whether the given courseid exists
    if ($courseid AND $courseid != SITEID) {
        require_course_login(get_course($courseid)); // This overwrites the object $COURSE .
    }

    $urltogo = $hobbycompletion->process_page();
    if ($urltogo !== null) {
        redirect($urltogo);
    }

    $PAGE->navbar->add(get_string('hobby:complete', 'hobby'));
    $PAGE->set_heading($course->fullname);
    $PAGE->set_title($hobby->name);
    $PAGE->set_pagelayout('incourse');

    echo $OUTPUT->header();
    echo $OUTPUT->heading(format_string($hobby->name));

    // Display the form with the questions.
    echo $hobbycompletion->render_items();

    echo $OUTPUT->footer();
}
catch (\Exception $e) {
    throw $e;
}

