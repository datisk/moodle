<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodle_stable';
$CFG->dbuser    = 'postgres';
$CFG->dbpass    = '123456';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 5432,
  'dbsocket' => '',
);

$CFG->wwwroot   = 'http://moodle-master.local';
$CFG->dataroot  = 'D:\\xampp\\htdocs\\moodledata';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

@error_reporting(E_ALL | E_STRICT); // NOT FOR PRODUCTION SERVERS!
@ini_set('display_errors', '1');    // NOT FOR PRODUCTION SERVERS!
$CFG->debug = (E_ALL | E_STRICT);   // === DEBUG_DEVELOPER - NOT FOR PRODUCTION SERVERS!
$CFG->debugdisplay = 1;             // NOT FOR PRODUCTION SERVERS!

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
$CFG->phpunit_prefix = 'phpu_';
$CFG->phpunit_dataroot = '/mytest';

$CFG->behat_wwwroot = 'http://moodle-behat.local';
$CFG->behat_prefix = 'bht_';
$CFG->behat_dataroot = 'D:\\xampp\\htdocs\\moodledata_behat';

$CFG->behat_profiles = [
        'default' => [
                'browser' => 'chrome',
                'extensions' => [
                        'Behat\MinkExtension' => [
                                'selenium2' => [
                                        'browser' => 'chrome',
                                ]
                        ]
                ]
        ]
];

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
